---
title: "404 Page Not Found"
description: "404 "
date: "2017-03-02"
categories:
    - "http"
    - "web"
---

**404** or **Page not Found** error message is an indication of that some web page you requested is not available on the [server](https://en.wikipedia.org/wiki/Web_server) or server is not able to provide anything related to the request you made.

This is a standard response code of HTTP ([Hypertext Transfer Protocol](https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol))
